﻿using Microsoft.AspNetCore.WebUtilities;
using System.Diagnostics;
using System.Reflection;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.ClientCaller
{
    public class ServiceCaller
    {
        private static HttpClient httpClient;
        public ServiceCaller(string baseUrl) {
            httpClient = new()
            {
                BaseAddress = new Uri(baseUrl),
            };
        }
        public async Task<RS> Get<RS>(string url, object? param)
        {
            if (param != null)
            {
                url = BuileQueryParam(url, param);
            }
            Debug.WriteLine(url);
            HttpResponseMessage response = await httpClient.GetAsync(url);
            RS jsonResponse = await response.Content.ReadFromJsonAsync<RS>();
            return jsonResponse;
        }
        public async Task<RS> Post<RQ, RS>(string url, RQ req, object? param){
            if(param != null) {
                url = BuileQueryParam(url, param);
            }
            HttpResponseMessage response = await httpClient.PostAsJsonAsync(url, req);
            RS jsonResponse = await response.Content.ReadFromJsonAsync<RS>();
            return jsonResponse;
        }
        public async Task<RS> Put<RQ, RS>(string url, RQ req, object? param)
        {
            if (param != null)
            {
                url = BuileQueryParam(url, param);
            }
            HttpResponseMessage response = await httpClient.PutAsJsonAsync(url, req);
            RS jsonResponse = await response.Content.ReadFromJsonAsync<RS>();
            return jsonResponse;
        }
        private string BuileQueryParam(string url, object parameters)
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            foreach (var property in parameters.GetType().GetProperties())
            {
                var jsonPropertyAttribute = property.GetCustomAttribute<JsonPropertyNameAttribute>();
                if (jsonPropertyAttribute != null)
                {
                    param[jsonPropertyAttribute.Name] = property.GetValue(parameters, null) != null ? property.GetValue(parameters, null).ToString() : "";
                }
            }

            return QueryHelpers.AddQueryString(url, param);
        }
    }
}
