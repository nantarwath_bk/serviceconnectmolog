﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConfirmPackedController
    {
        private readonly ILogger<ConfirmPackedController> _logger;
        private readonly ServiceCaller serviceCaller;
        public ConfirmPackedController(ILogger<ConfirmPackedController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/packed/by_job")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> SelectConfirmPackedByJob([FromQuery] SelectConfirmPackedByJobParameter parameters )
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/packed/by_job", parameters);
            return jsonResponse;
        }
    }
}
