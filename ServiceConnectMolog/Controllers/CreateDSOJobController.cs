﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CreateDSOJobController
    {
        private readonly ILogger<CreateDSOJobController> _logger;
        private readonly ServiceCaller serviceCaller;
        public CreateDSOJobController(ILogger<CreateDSOJobController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/dso/dso")]
        [HttpPost]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<CreateDSOJobRs> CreateDSOJob(CreateDSOJobRq req, [FromQuery] BaseParameters parameters)
        {
            CreateDSOJobRs jsonResponse = await serviceCaller.Post<CreateDSOJobRq, CreateDSOJobRs>("/dso/dso", req, parameters);
            return jsonResponse;
        }
    }
}
