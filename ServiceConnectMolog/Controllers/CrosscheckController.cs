﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CrosscheckController
    {
        private readonly ILogger<CrosscheckController> _logger;
        private readonly ServiceCaller serviceCaller;
        public CrosscheckController(ILogger<CrosscheckController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/crosscheck/asn")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> CrosscheckASN([FromQuery] SelectSummaryOfASNParameter parameters )
        {
            Object jsonResponse = await serviceCaller.Get<Object> ("/crosscheck/asn", parameters);
            return jsonResponse;
        }

        [Route("/crosscheck/dso")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> CrosscheckDSO([FromQuery] SelectSummaryOfDSOParameter parameters)
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/crosscheck/dso", parameters);
            return jsonResponse;
        }
    }
}
