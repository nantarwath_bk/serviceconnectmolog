﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InventoryController
    {
        private readonly ILogger<InventoryController> _logger;
        private readonly ServiceCaller serviceCaller;
        public InventoryController(ILogger<InventoryController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/inventory/list")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> Selectinventorylist([FromQuery] SelectinventorylistParameter parameters )
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/inventory/list", parameters);
            return jsonResponse;
        }
    }
}
