﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GoodsReceiveController
    {
        private readonly ILogger<GoodsReceiveController> _logger;
        private readonly ServiceCaller serviceCaller;
        public GoodsReceiveController(ILogger<GoodsReceiveController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/gr/by_job")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> SelectGoodsReceiveByJob([FromQuery] SelectGoodsReceiveByJobParameter parameters )
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/gr/by_job", parameters);
            return jsonResponse;
        }
        [Route("/v2/gr/by_job")]
        [HttpGet]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<Object> SelectGoodsReceiveByJobV2([FromQuery] SelectGoodsReceiveByJobV2Parameter parameters)
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/v2/gr/by_job", parameters);
            return jsonResponse;
        }
    }
}
