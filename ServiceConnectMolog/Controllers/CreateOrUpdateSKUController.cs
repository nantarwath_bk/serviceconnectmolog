﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CreateOrUpdateSKUController
    {
        private readonly ILogger<CreateOrUpdateSKUController> _logger;
        private readonly ServiceCaller serviceCaller;
        public CreateOrUpdateSKUController(ILogger<CreateOrUpdateSKUController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/sku/sku")]
        [HttpPost]
        /*[HttpPost(Name = "/sku/sku/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<CreateOrUpdateSKURs> CreateRrUpdateSKU(CreateOrUpdateSKURq req, [FromQuery] BaseParameters parameters)
        {
            CreateOrUpdateSKURs jsonResponse = await serviceCaller.Post<CreateOrUpdateSKURq, CreateOrUpdateSKURs>("/sku/sku", req, parameters);
            return jsonResponse;
        }
    }
}
