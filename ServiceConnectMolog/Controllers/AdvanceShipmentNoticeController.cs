﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AdvanceShipmentNoticeController
    {
        private readonly ILogger<AdvanceShipmentNoticeController> _logger;
        private readonly ServiceCaller serviceCaller;
        public AdvanceShipmentNoticeController(ILogger<AdvanceShipmentNoticeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/asn/asn")]
        [HttpPost]
        public async Task<CreateASNJobRs> CreateASNJob(CreateASNJobRq req, [FromQuery] BaseParameters parameters)
        {
            CreateASNJobRs jsonResponse = await serviceCaller.Post<CreateASNJobRq, CreateASNJobRs>("/asn/asn", req, parameters);
            return jsonResponse;
        }
        [Route("/asn/asn")]
        [HttpPut]
        public async Task<CreateASNJobRs> UpdateASNJob(CreateASNJobRq req, [FromQuery] BaseParameters parameters)
        {
            CreateASNJobRs jsonResponse = await serviceCaller.Put<CreateASNJobRq, CreateASNJobRs>("/asn/asn", req, parameters);
            return jsonResponse;
        }
    }
}
