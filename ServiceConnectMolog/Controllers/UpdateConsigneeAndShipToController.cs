﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UpdateConsigneeAndShipToController
    {
        private readonly ILogger<UpdateConsigneeAndShipToController> _logger;
        private readonly ServiceCaller serviceCaller;
        public UpdateConsigneeAndShipToController(ILogger<UpdateConsigneeAndShipToController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/dso/con_st")]
        [HttpPut]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<UpdateConsigneeAndShipToRs> UpdateConsigneeAndShipTo(UpdateConsigneeAndShipToRq req, [FromQuery] BaseParameters parameters)
        {
            UpdateConsigneeAndShipToRs jsonResponse = await serviceCaller.Put<UpdateConsigneeAndShipToRq, UpdateConsigneeAndShipToRs>("/dso/con_st", req, parameters);
            return jsonResponse;
        }
    }
}
