﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UpdateSKUBomController
    {
        private readonly ILogger<UpdateSKUBomController> _logger;
        private readonly ServiceCaller serviceCaller;
        public UpdateSKUBomController(ILogger<UpdateSKUBomController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/sku/skubom")]
        [HttpPut]
        /*[HttpPut(Name = "/sku/skubom/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<UpdateSKUBomRs> UpdateSKUBom(UpdateSKUBomRq req, [FromQuery] BaseParameters parameters)
        {
            UpdateSKUBomRs jsonResponse = await serviceCaller.Put<UpdateSKUBomRq, UpdateSKUBomRs>("/sku/skubom", req, parameters);
            return jsonResponse;
        }
    }
}
