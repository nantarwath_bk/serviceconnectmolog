﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CreatePSOJobController
    {
        private readonly ILogger<CreatePSOJobController> _logger;
        private readonly ServiceCaller serviceCaller;
        public CreatePSOJobController(ILogger<CreatePSOJobController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/pso/pso")]
        [HttpPost]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<CreatePSOJobRs> CreatePSOJob(CreatePSOJobRq req, [FromQuery] BaseParameters parameters)
        {
            CreatePSOJobRs jsonResponse = await serviceCaller.Post<CreatePSOJobRq, CreatePSOJobRs>("/pso/pso", req, parameters);
            return jsonResponse;
        }
    }
}
