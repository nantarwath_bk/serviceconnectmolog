﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UpdateInvoiceController
    {
        private readonly ILogger<UpdateInvoiceController> _logger;
        private readonly ServiceCaller serviceCaller;
        public UpdateInvoiceController(ILogger<UpdateInvoiceController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/dso/invoice")]
        [HttpPut]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<UpdateInvoiceRs> ConfirmPicked(UpdateInvoiceRq req, [FromQuery] BaseParameters parameters )
        {
            UpdateInvoiceRs jsonResponse = await serviceCaller.Put<UpdateInvoiceRq, UpdateInvoiceRs>("/dso/invoice", req, parameters);
            return jsonResponse;
        }
    }
}
