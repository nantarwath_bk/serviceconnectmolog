﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ConfirmPickedController
    {
        private readonly ILogger<ConfirmPickedController> _logger;
        private readonly ServiceCaller serviceCaller;
        public ConfirmPickedController(ILogger<ConfirmPickedController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/picked/by_job")]
        [HttpGet]
        public async Task<Object> SelectConfirmPickedByJob([FromQuery] SelectConfirmPickedByJobParameter parameters )
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/picked/by_job", parameters);
            return jsonResponse;
        }
        [Route("/v2/picked/by_job")]
        [HttpGet]
        public async Task<Object> SelectConfirmPickedByJobV2([FromQuery] SelectConfirmPickedByJobParameter parameters)
        {
            Object jsonResponse = await serviceCaller.Get<Object>("/v2/picked/by_job", parameters);
            return jsonResponse;
        }
    }
}
