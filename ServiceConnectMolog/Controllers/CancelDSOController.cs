﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CancelDSOController
    {
        private readonly ILogger<CancelDSOController> _logger;
        private readonly ServiceCaller serviceCaller;
        public CancelDSOController(ILogger<CancelDSOController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/dso/cancel")]
        [HttpPut]
        public async Task<CancelDSORs> CancelDSO(CancelDSORq req, [FromQuery] BaseParameters parameters)
        {
            CancelDSORs jsonResponse = await serviceCaller.Put<CancelDSORq, CancelDSORs>("/dso/cancel", req, parameters);
            return jsonResponse;
        }
    }
}
