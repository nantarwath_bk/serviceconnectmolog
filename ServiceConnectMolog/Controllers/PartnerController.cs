﻿using Microsoft.AspNetCore.Mvc;
using ServiceConnectMolog.ClientCaller;
using ServiceConnectMolog.Message;
using ServiceConnectMolog.Model.Parameters;

namespace ServiceConnectMolog.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PartnerController
    {
        private readonly ILogger<PartnerController> _logger;
        private readonly ServiceCaller serviceCaller;
        public PartnerController(ILogger<PartnerController> logger, IConfiguration configuration)
        {
            _logger = logger;
            serviceCaller = new ServiceCaller(configuration["ThirdParty:BaseUrl"]);
        }
        [Route("/partner/partner")]
        [HttpPost]
        /* [HttpPut(Name = "/partner/partner/{APP_KEY:string}/{TIMESTAMP:int}/{SIGN:string}")]*/
        public async Task<CreateOrUpdatePartnerRs> CreateDSOJob(CreateOrUpdatePartnerRq req, [FromQuery] BaseParameters parameters)
        {
            CreateOrUpdatePartnerRs jsonResponse = await serviceCaller.Post<CreateOrUpdatePartnerRq, CreateOrUpdatePartnerRs>("/partner/partner", req, parameters);
            return jsonResponse;
        }
    }
}
