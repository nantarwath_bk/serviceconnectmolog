﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateConsigneeAndShipToRq
    {
        [JsonPropertyName("<Release Reference Label-1>")]
        public string? releaseReferenceLabel1 { get; set; }

        [JsonPropertyName("<Release Reference Label-2>")]
        public string? releaseReferenceLabel2 { get; set; }

        [JsonPropertyName("<Release Reference Label-3>")]
        public string? releaseReferenceLabel3 { get; set; }

        [JsonPropertyName("<Release Reference Label-4>")]
        public string? releaseReferenceLabel4 { get; set; }

        [JsonPropertyName("CON_CODE")]
        public string? conCode { get; set; }

        [JsonPropertyName("CON_COMPANY")]
        public string? conCompany { get; set; }

        [JsonPropertyName("CON_TAX_NO")]
        public string? conTaxNo { get; set; }

        [JsonPropertyName("CON_BRANCH")]
        public string? conBranch { get; set; }

        [JsonPropertyName("CON_ADDRESS")]
        public string? conAddress { get; set; }

        [JsonPropertyName("CON_ADDRESS2")]
        public string? conAddress2 { get; set; }

        [JsonPropertyName("CON_AREA1")]
        public string? conArea1 { get; set; }

        [JsonPropertyName("CON_AREA2")]
        public string? conArea2 { get; set; }

        [JsonPropertyName("CON_AREA3")]
        public string? conArea3 { get; set; }

        [JsonPropertyName("CON_AREA4")]
        public string? conArea4 { get; set; }

        [JsonPropertyName("CON_ZONE1")]
        public string? conZone1 { get; set; }

        [JsonPropertyName("CON_ZONE2")]
        public string? conZone2 { get; set; }

        [JsonPropertyName("CON_REGION1")]
        public string? conRegion1 { get; set; }

        [JsonPropertyName("CON_REGION2")]
        public string? conRegion2 { get; set; }

        [JsonPropertyName("CON_COUNTRY")]
        public string? conCountry { get; set; }

        [JsonPropertyName("CON_ZIP")]
        public string? conZip { get; set; }

        [JsonPropertyName("CON_CONTACT_NAME")]
        public string? conContactName { get; set; }

        [JsonPropertyName("CON_CONTACT_PHONE")]
        public string? conContactPhone { get; set; }

        [JsonPropertyName("CON_CONTACT_EMAIL")]
        public string? conContactEmail { get; set; }

        [JsonPropertyName("CON_NOTE")]
        public string? conNote { get; set; }

        [JsonPropertyName("ST_CODE")]
        public string? stCode { get; set; }

        [JsonPropertyName("ST_COMPANY")]
        public string? stCompany { get; set; }

        [JsonPropertyName("ST_ADDRESS")]
        public string? stAddress { get; set; }

        [JsonPropertyName("ST_ADDRESS2")]
        public string? stAddress2 { get; set; }

        [JsonPropertyName("ST_AREA1")]
        public string? stArea1 { get; set; }

        [JsonPropertyName("ST_AREA2")]
        public string? stArea2 { get; set; }

        [JsonPropertyName("ST_AREA3")]
        public string? stArea3 { get; set; }

        [JsonPropertyName("ST_AREA4")]
        public string? stArea4 { get; set; }

        [JsonPropertyName("ST_ZONE1")]
        public string? stZone1 { get; set; }

        [JsonPropertyName("ST_ZONE2")]
        public string? stZone2 { get; set; }

        [JsonPropertyName("ST_REGION1")]
        public string? stRegion1 { get; set; }

        [JsonPropertyName("ST_REGION2")]
        public string? stRegion2 { get; set; }

        [JsonPropertyName("ST_COUNTRY")]
        public string? stCountry { get; set; }

        [JsonPropertyName("ST_ZIP")]
        public string? stZip { get; set; }

        [JsonPropertyName("ST_CONTACT_NAME")]
        public string? stContactName { get; set; }

        [JsonPropertyName("ST_CONTACT_PHONE")]
        public string? stContactPhone { get; set; }

        [JsonPropertyName("ST_CONTACT_EMAIL")]
        public string? stContactEmail { get; set; }

        [JsonPropertyName("ST_NOTE")]
        public string? stNote { get; set; }


    }
}
