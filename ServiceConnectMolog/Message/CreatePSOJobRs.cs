﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreatePSOJobRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
