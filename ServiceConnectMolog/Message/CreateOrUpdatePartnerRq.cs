﻿using ServiceConnectMolog.Model;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateOrUpdatePartnerRq
    {
        [JsonPropertyName("DATA")]
        public CreateOrUpdatePartnerRqData[] data { get; set; }
    }
}
