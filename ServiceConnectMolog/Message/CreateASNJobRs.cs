﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateASNJobRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}