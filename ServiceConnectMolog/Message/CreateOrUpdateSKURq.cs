﻿using ServiceConnectMolog.Model;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateOrUpdateSKURq
    {
        [JsonPropertyName("DATA")]
        public CreateOrUpdateSKURqData[] data { set; get; }
    }
}
