﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateOrUpdateSKURs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
