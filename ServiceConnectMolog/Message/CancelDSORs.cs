﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CancelDSORs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
