﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateConsigneeAndShipToRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
