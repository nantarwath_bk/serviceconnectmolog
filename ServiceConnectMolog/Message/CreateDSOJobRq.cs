﻿using ServiceConnectMolog.Model;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateDSOJobRq
    {
        [JsonPropertyName("<Release Reference Label-1>")]
        public string? releaseReferenceLabel1 { get; set; }

        [JsonPropertyName("<Release Reference Label-2>")]
        public string? releaseReferenceLabel2 { get; set; }

        [JsonPropertyName("<Release Reference Label-3>")]
        public string? releaseReferenceLabel3 { get; set; }

        [JsonPropertyName("<Release Reference Label-4>")]
        public string? releaseReferenceLabel4 { get; set; }

        [JsonPropertyName("REL_PREFIX")]
        public string? relPrefix { get; set; }

        [JsonPropertyName("TEAM")]
        public string? team { get; set; }

        [JsonPropertyName("EXP_RELEASE_DATE")]
        public DateTime? expReleaseDate { get; set; }

        [JsonPropertyName("INVOICE_NO")]
        public string? invoiceNo { get; set; }

        [JsonPropertyName("TP_VEHICLE_NO")]
        public string? tpVehicleNo { get; set; }

        [JsonPropertyName("TP_BOOKING")]
        public string? tpBooking { get; set; }

        [JsonPropertyName("TP_CARRIER")]
        public string? tpCarrier { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_1")]
        public string? tpContainerNo1 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_1")]
        public string? tpContainerType1 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_1")]
        public string? tpSealNo1 { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_2")]
        public string? tpContainerNo2 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_2")]
        public string? tpContainerType2 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_2")]
        public string? tpSealNo2 { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_3")]
        public string? tpContainerNo3 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_3")]
        public string? tpContainerType3 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_3")]
        public string? tpSealNo3 { get; set; }

        [JsonPropertyName("TP_AIRWAYBILL")]
        public string? tpAirwaybill { get; set; }

        [JsonPropertyName("TP_BILL_OF_LANDING")]
        public string? tpBillOfLanding { get; set; }

        [JsonPropertyName("TP_ORIGIN")]
        public string? tpOrigin { get; set; }

        [JsonPropertyName("TP_VESSEL")]
        public string? tpVessel { get; set; }

        [JsonPropertyName("TP_PORT_OF_LANDING")]
        public string? tpPortOfLanding { get; set; }

        [JsonPropertyName("TP_PORT_DISCHARGE")]
        public string? tpPortDischarge { get; set; }

        [JsonPropertyName("TP_VOYAGE")]
        public string? tpVoyage { get; set; }

        [JsonPropertyName("TP_ETD")]
        public DateTime? tpEtd { get; set; }

        [JsonPropertyName("CON_CODE")]
        public string? conCode { get; set; }

        [JsonPropertyName("CON_COMPANY")]
        public string? conCompany { get; set; }

        [JsonPropertyName("CON_TAX_NO")]
        public string? conTaxNo { get; set; }

        [JsonPropertyName("CON_BRANCH")]
        public string? conBranch { get; set; }

        [JsonPropertyName("CON_ADDRESS")]
        public string? conAddress { get; set; }

        [JsonPropertyName("CON_ADDRESS2")]
        public string? conAddress2 { get; set; }

        [JsonPropertyName("CON_AREA1")]
        public string? conArea1 { get; set; }

        [JsonPropertyName("CON_AREA2")]
        public string? conArea2 { get; set; }

        [JsonPropertyName("CON_AREA3")]
        public string? conArea3 { get; set; }

        [JsonPropertyName("CON_AREA4")]
        public string? conArea4 { get; set; }

        [JsonPropertyName("CON_ZONE1")]
        public string? conZone1 { get; set; }

        [JsonPropertyName("CON_ZONE2")]
        public string? conZone2 { get; set; }

        [JsonPropertyName("CON_REGION1")]
        public string? conRegion1 { get; set; }

        [JsonPropertyName("CON_REGION2")]
        public string? conRegion2 { get; set; }

        [JsonPropertyName("CON_COUNTRY")]
        public string? conCountry { get; set; }

        [JsonPropertyName("CON_ZIP")]
        public string? conZip { get; set; }

        [JsonPropertyName("CON_CONTACT_NAME")]
        public string? conContactName { get; set; }

        [JsonPropertyName("CON_CONTACT_PHONE")]
        public string? conContactPhone { get; set; }

        [JsonPropertyName("CON_CONTACT_EMAIL")]
        public string? conContactEmail { get; set; }

        [JsonPropertyName("CON_NOTE")]
        public string? conNote { get; set; }

        [JsonPropertyName("ST_CODE")]
        public string? stCode { get; set; }

        [JsonPropertyName("ST_COMPANY")]
        public string? stCompany { get; set; }

        [JsonPropertyName("ST_ADDRESS")]
        public string? stAddress { get; set; }

        [JsonPropertyName("ST_ADDRESS2")]
        public string? stAddress2 { get; set; }

        [JsonPropertyName("ST_AREA1")]
        public string? stArea1 { get; set; }

        [JsonPropertyName("ST_AREA2")]
        public string? stArea2 { get; set; }

        [JsonPropertyName("ST_AREA3")]
        public string? stArea3 { get; set; }

        [JsonPropertyName("ST_AREA4")]
        public string? stArea4 { get; set; }

        [JsonPropertyName("ST_ZONE1")]
        public string? stZone1 { get; set; }

        [JsonPropertyName("ST_ZONE2")]
        public string? stZone2 { get; set; }

        [JsonPropertyName("ST_REGION1")]
        public string? stRegion1 { get; set; }

        [JsonPropertyName("ST_REGION2")]
        public string? stRegion2 { get; set; }

        [JsonPropertyName("ST_COUNTRY")]
        public string? stCountry { get; set; }

        [JsonPropertyName("ST_ZIP")]
        public string? stZip { get; set; }

        [JsonPropertyName("ST_CONTACT_NAME")]
        public string? stContactName { get; set; }

        [JsonPropertyName("ST_CONTACT_PHONE")]
        public string? stContactPhone { get; set; }

        [JsonPropertyName("ST_CONTACT_EMAIL")]
        public string? stContactEmail { get; set; }

        [JsonPropertyName("ST_NOTE")]
        public string? stNote { get; set; }

        [JsonPropertyName("DEL_CONSIGNMENT")]
        public string? delConsignment { get; set; }

        [JsonPropertyName("DEL_URGENT_ORDER")]
        public bool? delUrgentOrder { get; set; }

        [JsonPropertyName("DEL_COD")]
        public bool? delCod { get; set; }

        [JsonPropertyName("DEL_COD_AMOUNT")]
        public float? delCodAmount { get; set; }

        [JsonPropertyName("DELIVERY_TIME_FROM")]
        public string? deliveryTimeFrom { get; set; } //time patern?

        [JsonPropertyName("DELIVERY_TIME_TO")]
        public string? deliveryTimeTo { get; set; } //time patern?

        [JsonPropertyName("SHIPPING_INSTRUCTION")]
        public string? shippingInstruction { get; set; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { get; set; }

        [JsonPropertyName("BONDED")]
        public bool? bonded { get; set; }

        [JsonPropertyName("TOTAL_PALLET")]
        public int? totalPallet { get; set; }

        [JsonPropertyName("STORAGE_CHARGE")]
        public bool? storageCharge { get; set; }

        [JsonPropertyName("IMG_TRUCK_01"), DefaultValue(null)]
        public string? imgTruck01 { get; set; }

        [JsonPropertyName("IMG_TRUCK_02"), DefaultValue(null)]
        public string? imgTruck02 { get; set; }

        [JsonPropertyName("IMG_TRUCK_03"), DefaultValue(null)]
        public string? imgTruck03 { get; set; }

        [JsonPropertyName("IMG_TRUCK_04"), DefaultValue(null)]
        public string? imgTruck04 { get; set; }

        [JsonPropertyName("IMG_TRUCK_05"), DefaultValue(null)]
        public string? imgTruck05 { get; set; }

        [JsonPropertyName("IMG_TRUCK_06"), DefaultValue(null)]
        public string? imgTruck06 { get; set; }

        [JsonPropertyName("IMG_TRUCK_07"), DefaultValue(null)]
        public string? imgTruck07 { get; set; }

        [JsonPropertyName("IMG_TRUCK_08"), DefaultValue(null)]
        public string? imgTruck08 { get; set; }

        [JsonPropertyName("IMG_TRUCK_09"), DefaultValue(null)]
        public string? imgTruck09 { get; set; }

        [JsonPropertyName("IMG_TRUCK_10"), DefaultValue(null)]
        public string? imgTruck10 { get; set; }

        [JsonPropertyName("IMG_GOODS_01"), DefaultValue(null)]
        public string? imgGoods01 { get; set; }

        [JsonPropertyName("IMG_GOODS_02"), DefaultValue(null)]
        public string? imgGoods02 { get; set; }

        [JsonPropertyName("IMG_GOODS_03"), DefaultValue(null)]
        public string? imgGoods03 { get; set; }

        [JsonPropertyName("IMG_GOODS_04"), DefaultValue(null)]
        public string? imgGoods04 { get; set; }

        [JsonPropertyName("IMG_GOODS_05"), DefaultValue(null)]
        public string? imgGoods05 { get; set; }

        [JsonPropertyName("IMG_GOODS_06"), DefaultValue(null)]
        public string? imgGoods06 { get; set; }

        [JsonPropertyName("IMG_GOODS_07"), DefaultValue(null)]
        public string? imgGoods07 { get; set; }

        [JsonPropertyName("IMG_GOODS_08"), DefaultValue(null)]
        public string? imgGoods08 { get; set; }

        [JsonPropertyName("IMG_GOODS_09"), DefaultValue(null)]
        public string? imgGoods09 { get; set; }

        [JsonPropertyName("IMG_GOODS_10"), DefaultValue(null)]
        public string? imgGoods10 { get; set; }

        [JsonPropertyName("<Release Header User Defined-01>")]
        public string? releaseHeaderUserDefined01 { get; set; }

        [JsonPropertyName("<Release Header User Defined-02>")]
        public string? releaseHeaderUserDefined02 { get; set; }

        [JsonPropertyName("<Release Header User Defined-03>")]
        public string? releaseHeaderUserDefined03 { get; set; }

        [JsonPropertyName("<Release Header User Defined-04>")]
        public string? releaseHeaderUserDefined04 { get; set; }

        [JsonPropertyName("<Release Header User Defined-05>")]
        public string? releaseHeaderUserDefined05 { get; set; }

        [JsonPropertyName("<Release Header User Defined-06>")]
        public string? releaseHeaderUserDefined06 { get; set; }

        [JsonPropertyName("<Release Header User Defined-07>")]
        public string? releaseHeaderUserDefined07 { get; set; }

        [JsonPropertyName("<Release Header User Defined-08>")]
        public string? releaseHeaderUserDefined08 { get; set; }

        [JsonPropertyName("<Release Header User Defined-09>")]
        public string? releaseHeaderUserDefined09 { get; set; }

        [JsonPropertyName("<Release Header User Defined-10>")]
        public string? releaseHeaderUserDefined10 { get; set; }

        [JsonPropertyName("<Release Header User Defined-11>")]
        public string? releaseHeaderUserDefined11 { get; set; }

        [JsonPropertyName("<Release Header User Defined-12>")]
        public string? releaseHeaderUserDefined12 { get; set; }

        [JsonPropertyName("<Release Header User Defined-13>")]
        public string? releaseHeaderUserDefined13 { get; set; }

        [JsonPropertyName("<Release Header User Defined-14>")]
        public string? releaseHeaderUserDefined14 { get; set; }

        [JsonPropertyName("<Release Header User Defined-15>")]
        public string? releaseHeaderUserDefined15 { get; set; }

        [JsonPropertyName("DETAIL")]
        public CreateDSOJobRqDetail[] detail { get; set; }

    }
}
