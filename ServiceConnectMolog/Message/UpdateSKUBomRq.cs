﻿using ServiceConnectMolog.Model;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateSKUBomRq
    {
        [JsonPropertyName("DATA")]
        public UpdateSKUBomRqData[] data { get; set; }
    }
}
