﻿using ServiceConnectMolog.Model;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateInvoiceRq
    {
        [JsonPropertyName("<Release Reference Label-1>")]
        public string? releaseReferenceLabel1 { get; set; }

        [JsonPropertyName("<Release Reference Label-2>")]
        public string? releaseReferenceLabel2 { get; set; }

        [JsonPropertyName("<Release Reference Label-3>")]
        public string? releaseReferenceLabel3 { get; set; }

        [JsonPropertyName("<Release Reference Label-4>")]
        public string? releaseReferenceLabel4 { get; set; }

        [JsonPropertyName("INVOICE_TYPE")]
        public string invoiceType { get; set; }

        [JsonPropertyName("INVOICE_NO")]
        public string invoiceNo { get; set; }

        [JsonPropertyName("INVOICE_FILE")]
        public string? invoiceFile { get; set; }

        [JsonPropertyName("INVOICE_DATA")]
        public UpdateInvoiceRqInvoiceData? invoiceData { get; set; }
    }
}
