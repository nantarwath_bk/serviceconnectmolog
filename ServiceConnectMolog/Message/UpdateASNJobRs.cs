﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateASNJobRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}