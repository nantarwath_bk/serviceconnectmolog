﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateDSOJobRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
