﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateOrUpdatePartnerRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
