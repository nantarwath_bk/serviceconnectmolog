﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateSKUBomRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
