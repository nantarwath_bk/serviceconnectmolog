﻿using ServiceConnectMolog.Model;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class CreateASNJobRq
    {
        [JsonPropertyName("<Receive Reference Label-1>")]
        public string? receiveReferenceLabel1 { get; set; }

        [JsonPropertyName("<Receive Reference Label-2>")]
        public string? receiveReferenceLabel2 { get; set; }

        [JsonPropertyName("<Receive Reference Label-3>")]
        public string? receiveReferenceLabel3 { get; set; }

        [JsonPropertyName("<Receive Reference Label-4>")]
        public string? receiveReferenceLabel4 { get; set; }

        [JsonPropertyName("EXP_RECEIVED_DATE")]
        public DateTime expReceivedDate { get; set; }

        [JsonPropertyName("TP_VEHICLE_NO")]
        public string? tpVehicleNo { get; set; }

        [JsonPropertyName("TP_CARRIER")]
        public string? tpCarrier { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_1")]
        public string? tpContainerNo1 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_1")]
        public string? tpContainerType1 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_1")]
        public string? tpSealNo1 { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_2")]
        public string? tpContainerNo2 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_2")]
        public string? tpContainerType2 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_2")]
        public string? tpSealNo2 { get; set; }

        [JsonPropertyName("TP_CONTAINER_NO_3")]
        public string? tpContainerNo3 { get; set; }

        [JsonPropertyName("TP_CONTAINER_TYPE_3")]
        public string? tpContainerType3 { get; set; }

        [JsonPropertyName("TP_SEAL_NO_3")]
        public string? tpSealNo3 { get; set; }

        [JsonPropertyName("TP_AIRWAYBILL")]
        public string? tpAirwaybill { get; set; }

        [JsonPropertyName("TP_BILL_OF_LANDING")]
        public string? tpBillOfLanding { get; set; }

        [JsonPropertyName("TP_ORIGIN")]
        public string? tpOrigin { get; set; }

        [JsonPropertyName("TP_VESSEL")]
        public string? tpVessel { get; set; }

        [JsonPropertyName("TP_PORT_OF_LANDING")]
        public string? tpPortOfLanding { get; set; }

        [JsonPropertyName("TP_PORT_DISCHARGE")]
        public string? tpPortDischarge { get; set; }

        [JsonPropertyName("TP_VOYAGE")]
        public string? tpVoyage { get; set; }

        [JsonPropertyName("TP_ETA")]
        public DateTime? tpEta { get; set; }

        [JsonPropertyName("SUP_CODE")]
        public string? supCode { get; set; }

        [JsonPropertyName("SUP_COMPANY")]
        public string? supCompany { get; set; }

        [JsonPropertyName("SUP_ADDRESS")]
        public string? supAddress { get; set; }

        [JsonPropertyName("SUP_ADDRESS2")]
        public string? supAddress2 { get; set; }

        [JsonPropertyName("SUP_AREA1")]
        public string? supArea1 { get; set; }

        [JsonPropertyName("SUP_AREA2")]
        public string? supArea2 { get; set; }

        [JsonPropertyName("SUP_AREA3")]
        public string? supArea3 { get; set; }

        [JsonPropertyName("SUP_AREA4")]
        public string? supArea4 { get; set; }

        [JsonPropertyName("SUP_ZONE1")]
        public string? supZone1 { get; set; }

        [JsonPropertyName("SUP_ZONE2")]
        public string? supZone2 { get; set; }

        [JsonPropertyName("SUP_REGION1")]
        public string? supRegion1 { get; set; }

        [JsonPropertyName("SUP_REGION2")]
        public string? supRegion2 { get; set; }

        [JsonPropertyName("SUP_COUNTRY")]
        public string? supCountry { get; set; }

        [JsonPropertyName("SUP_ZIP")]
        public string? supZip { get; set; }

        [JsonPropertyName("SUP_CONTACT_NAME")]
        public string? supContactName { get; set; }

        [JsonPropertyName("SUP_CONTACT_PHONE")]
        public string? supContactPhone { get; set; }

        [JsonPropertyName("SUP_CONTACT_EMAIL")]
        public string? supContactEmail { get; set; }

        [JsonPropertyName("SUP_NOTE")]
        public string? supNote { get; set; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { get; set; }

        [JsonPropertyName("<Receive Header User Defined-01>")]
        public string? receiveHeaderUserDefined01 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-02>")]
        public string? receiveHeaderUserDefined02 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-03>")]
        public string? receiveHeaderUserDefined03 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-04>")]
        public string? receiveHeaderUserDefined04 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-05>")]
        public string? receiveHeaderUserDefined05 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-06>")]
        public string? receiveHeaderUserDefined06 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-07>")]
        public string? receiveHeaderUserDefined07 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-08>")]
        public string? receiveHeaderUserDefined08 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-09>")]
        public string? receiveHeaderUserDefined09 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-10>")]
        public string? receiveHeaderUserDefined10 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-11>")]
        public string? receiveHeaderUserDefined11 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-12>")]
        public string? receiveHeaderUserDefined12 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-13>")]
        public string? receiveHeaderUserDefined13 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-14>")]
        public string? receiveHeaderUserDefined14 { get; set; }

        [JsonPropertyName("<Receive Header User Defined-15>")]
        public string? receiveHeaderUserDefined15 { get; set; }

        [JsonPropertyName("DETAIL")]
        public CreateASNJobRqDetail[]? detail { get; set; }

    }
}
