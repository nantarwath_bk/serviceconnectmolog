﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Message
{
    public class UpdateInvoiceRs
    {
        [JsonPropertyName("STATUS")]
        public string? status { get; set; }
    }
}
