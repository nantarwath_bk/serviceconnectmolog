﻿using System.ComponentModel;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdateSKURqData
    {
        [JsonPropertyName("SKU_CODE")]
        public string skuCode { set; get; }

        [JsonPropertyName("ACTIVE")]
        public bool active { set; get; }

        [JsonPropertyName("STORAGE_TYPE")]
        public string storageType { set; get; }

        [JsonPropertyName("STORAGE_CAT_SERVICE")]
        public string storageCatService { set; get; }

        [JsonPropertyName("PRODUCT_TYPE")]
        public string productType { set; get; }

        [JsonPropertyName("PICK_BY_CODE")]
        public string pickByCode { set; get; }

        [JsonPropertyName("CBM_RATIO")]
        public float cbmRatio { set; get; }

        [JsonPropertyName("BOM_SKU")]
        public bool bomSku { set; get; }

        [JsonPropertyName("DEFAULT_EXPIRY")]
        public bool defaultExpiry { set; get; }

        [JsonPropertyName("SKU_DESC")]
        public string? skuDesc { set; get; }

        [JsonPropertyName("SKU_DESC_LOCAL")]
        public string? skuDescLocal { set; get; }

        [JsonPropertyName("MATERIAL_TYPE")]
        public string? materialType { set; get; }

        [JsonPropertyName("PRODUCT_FAMILY_01")]
        public string? productFamily01 { set; get; }

        [JsonPropertyName("PRODUCT_FAMILY_02")]
        public string? productFamily02 { set; get; }

        [JsonPropertyName("PRODUCT_FAMILY_03")]
        public string? productFamily03 { set; get; }

        [JsonPropertyName("ABC_VALUE")]
        public string? abcValue { set; get; }

        [JsonPropertyName("ABC_TRANSACTION")]
        public string? abcTransaction { set; get; }

        [JsonPropertyName("EXPIRY_FROM")]
        public string? expiryFrom { set; get; }

        [JsonPropertyName("EXPIRY_DAYS")]
        public int? expiryDays { set; get; }

        [JsonPropertyName("EXPIRY_PERIOD")]
        public string? expiryPeriod { set; get; }

        [JsonPropertyName("DEFAULT_MANF")]
        public bool? defaultManf { set; get; }

        [JsonPropertyName("MANUF_FROM")]
        public string? manufFrom { set; get; }

        [JsonPropertyName("MANUF_DAYS")]
        public int? manufDays { set; get; }

        [JsonPropertyName("MANUF_PERIOD")]
        public string? manufPeriod { set; get; }

        [JsonPropertyName("LAST_CC_DATE")]
        public DateTime? lastCcDate { set; get; }

        [JsonPropertyName("MIN_STOCK_QTY")]
        public float? minStockQty { set; get; }

        [JsonPropertyName("MIN_REORDER_QTY")]
        public float? minReorderQty { set; get; }

        [JsonPropertyName("MAX_STOCK_QTY")]
        public float? maxStockQty { set; get; }

        [JsonPropertyName("MIN_SHELF_LIFE")]
        public float? minShelfLife { set; get; }

        [JsonPropertyName("DAY_PREPARE_SHIP")]
        public int? dayPrepareShip { set; get; }

        [JsonPropertyName("REQUIRE_SCAN_IN_SERIAL")]
        public bool? requireScanInSerial { set; get; }

        [JsonPropertyName("REQUIRE_SCAN_OUT_SERIAL")]
        public bool? requireScanOutSerial { set; get; }

        [JsonPropertyName("SKU_IMAGE_1"), DefaultValue(null)]
        public string? skuImage1 { set; get; }

        [JsonPropertyName("SKU_IMAGE_2"), DefaultValue(null)]
        public string? skuImage2 { set; get; }

        [JsonPropertyName("SKU_IMAGE_3"), DefaultValue(null)]
        public string? skuImage3 { set; get; }

        [JsonPropertyName("SKU_IMAGE_4"), DefaultValue(null)]
        public string? skuImage4 { set; get; }

        [JsonPropertyName("SKU_IMAGE_5"), DefaultValue(null)]
        public string? skuImage5 { set; get; }

        [JsonPropertyName("SKU_IMAGE_6"), DefaultValue(null)]
        public string? skuImage6 { set; get; }

        [JsonPropertyName("SKU_IMAGE_7"), DefaultValue(null)]
        public string? skuImage7 { set; get; }

        [JsonPropertyName("SKU_IMAGE_8"), DefaultValue(null)]
        public string? skuImage8 { set; get; }

        [JsonPropertyName("SKU_IMAGE_9"), DefaultValue(null)]
        public string? skuImage9 { set; get; }

        [JsonPropertyName("SKU_IMAGE_10"), DefaultValue(null)]
        public string? skuImage10 { set; get; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { set; get; }

        [JsonPropertyName("COLOUR")]
        public string? colour { set; get; }

        [JsonPropertyName("STYLE")]
        public string? style { set; get; }

        [JsonPropertyName("SKU_ADS_DETAIL")]
        public string? skuAdsDetail { set; get; }

        [JsonPropertyName("RECEIVE_MIN_DAYS")]
        public int? receiveMindays { set; get; }

        [JsonPropertyName("<SKU User Defined-01>")]
        public string? skuUserDefined01 { set; get; }

        [JsonPropertyName("<SKU User Defined-02>")]
        public string? skuUserDefined02 { set; get; }

        [JsonPropertyName("<SKU User Defined-03>")]
        public string? skuUserDefined03 { set; get; }

        [JsonPropertyName("<SKU User Defined-04>")]
        public string? skuUserDefined04 { set; get; }

        [JsonPropertyName("<SKU User Defined-05>")]
        public string? skuUserDefined05 { set; get; }

        [JsonPropertyName("<SKU User Defined-06>")]
        public string? skuUserDefined06 { set; get; }

        [JsonPropertyName("<SKU User Defined-07>")]
        public string? skuUserDefined07 { set; get; }

        [JsonPropertyName("<SKU User Defined-08>")]
        public string? skuUserDefined08 { set; get; }

        [JsonPropertyName("<SKU User Defined-09>")]
        public string? skuUserDefined09 { set; get; }

        [JsonPropertyName("<SKU User Defined-10>")]
        public string? skuUserDefined10 { set; get; }

        [JsonPropertyName("DIMENSION")]
        public CreateOrUpdateSKURqDataDimension[] dimension { set; get; }

        [JsonPropertyName("BOM")]
        public CreateOrUpdateSKURqDataBom[]? bom { set; get; }

    }
}
