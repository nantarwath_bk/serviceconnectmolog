﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdateSKURqDataDimensionBarcode
    {
        [JsonPropertyName("SKU_REF_TYPE")]
        public string sukRefType { set; get; }

        [JsonPropertyName("SKU_REF_BARCODE")]
        public string skuRefBarcode { set; get; }


        [JsonPropertyName("SKU_REF_CODE")]
        public string? skuRefCode { set; get; }


        [JsonPropertyName("GS1_EXTENSION")]
        public string? sg1Extension { set; get; }

    }
}
