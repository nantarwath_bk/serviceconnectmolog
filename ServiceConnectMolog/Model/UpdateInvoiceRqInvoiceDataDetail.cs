﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class UpdateInvoiceRqInvoiceDataDetail
    {
        [JsonPropertyName("SEQUENCE")]
        public int? sequence { get; set; }

        [JsonPropertyName("ITEM_CODE")]
        public string? itemCode { get; set; }

        [JsonPropertyName("ITEM_DESC")]
        public string? itemDesc { get; set; }

        [JsonPropertyName("PACK_CODE")]
        public string? packCode { get; set; }

        [JsonPropertyName("UNIT_PRICE")]
        public float? unitPrice { get; set; }

        [JsonPropertyName("QUANTITY")]
        public float? quantity { get; set; }

        [JsonPropertyName("DISCOUNT_AMOUNT")]
        public float? discountAmount { get; set; }

        [JsonPropertyName("DISCOUNT_PERCENT")]
        public float? discountPercent { get; set; }

        [JsonPropertyName("DISCOUNT_APPLIED_BEFORE_TAX")]
        public bool? discountAppliedBeforeTax { get; set; }

        [JsonPropertyName("AMOUNT_BEFORE_TAX")]
        public float? amountBeforeTax { get; set; }

        [JsonPropertyName("TAX_CODE")]
        public string? taxCode { get; set; }

        [JsonPropertyName("TAX_PERCENT")]
        public float? taxPercent { get; set; }

        [JsonPropertyName("LINE_AMOUNT_BEFORE_DISCOUNT")]
        public float? lineAmountBeforeDiscount { get; set; }

        [JsonPropertyName("INVOICE_DISCOUNT_ALLOCATION")]
        public float? invoiceDiscountAllocation { get; set; }

        [JsonPropertyName("LINE_AMOUNT_BEFORE_TAX")]
        public float? lineAmountBeforeTax { get; set; }

        [JsonPropertyName("LINE_TAX_AMOUNT")]
        public float? lineTaxAmount { get; set; }

        [JsonPropertyName("LINE_AMOUNT_INCLUDING_TAX")]
        public float? lineAmountIncludingTax { get; set; }

        [JsonPropertyName("<Release User Defined-01>")]
        public string? releaseUserDefined01 { get; set; }

        [JsonPropertyName("<Release User Defined-02>")]
        public string? releaseUserDefined02 { get; set; }

        [JsonPropertyName("<Release User Defined-03>")]
        public string? releaseUserDefined03 { get; set; }

        [JsonPropertyName("<Release User Defined-04>")]
        public string? releaseUserDefined04 { get; set; }

        [JsonPropertyName("<Release User Defined-05>")]
        public string? releaseUserDefined05 { get; set; }

        [JsonPropertyName("<Release User Defined-06>")]
        public string? releaseUserDefined06 { get; set; }

        [JsonPropertyName("<Release User Defined-07>")]
        public string? releaseUserDefined07 { get; set; }

        [JsonPropertyName("<Release User Defined-08>")]
        public string? releaseUserDefined08 { get; set; }

        [JsonPropertyName("<Release User Defined-09>")]
        public string? releaseUserDefined09 { get; set; }

        [JsonPropertyName("<Release User Defined-10>")]
        public string? releaseUserDefined10 { get; set; }

        [JsonPropertyName("<Release User Defined-11>")]
        public string? releaseUserDefined11 { get; set; }

        [JsonPropertyName("<Release User Defined-12>")]
        public string? releaseUserDefined12 { get; set; }

        [JsonPropertyName("<Release User Defined-13>")]
        public string? releaseUserDefined13 { get; set; }

        [JsonPropertyName("<Release User Defined-14>")]
        public string? releaseUserDefined14 { get; set; }

        [JsonPropertyName("<Release User Defined-15>")]
        public string? releaseUserDefined15 { get; set; }

        [JsonPropertyName("<Release User Defined-16>")]
        public string? releaseUserDefined16 { get; set; }

        [JsonPropertyName("<Release User Defined-17>")]
        public string? releaseUserDefined17 { get; set; }

        [JsonPropertyName("<Release User Defined-18>")]
        public string? releaseUserDefined18 { get; set; }

        [JsonPropertyName("<Release User Defined-19>")]
        public string? releaseUserDefined19 { get; set; }

        [JsonPropertyName("<Release User Defined-20>")]
        public string? releaseUserDefined20 { get; set; }

        [JsonPropertyName("<Release User Defined-21>")]
        public string? releaseUserDefined21 { get; set; }

        [JsonPropertyName("<Release User Defined-22>")]
        public string? releaseUserDefined22 { get; set; }

        [JsonPropertyName("<Release User Defined-23>")]
        public string? releaseUserDefined23 { get; set; }

        [JsonPropertyName("<Release User Defined-24>")]
        public string? releaseUserDefined24 { get; set; }

        [JsonPropertyName("<Release User Defined-25>")]
        public string? releaseUserDefined25 { get; set; }

        [JsonPropertyName("<Release User Defined-26>")]
        public string? releaseUserDefined26 { get; set; }

        [JsonPropertyName("<Release User Defined-27>")]
        public string? releaseUserDefined27 { get; set; }

        [JsonPropertyName("<Release User Defined-28>")]
        public string? releaseUserDefined28 { get; set; }

        [JsonPropertyName("<Release User Defined-29>")]
        public string? releaseUserDefined29 { get; set; }

        [JsonPropertyName("<Release User Defined-30>")]
        public string? releaseUserDefined30 { get; set; }

    }
}
