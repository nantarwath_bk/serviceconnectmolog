﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdatePartnerRqData
    {
        [JsonPropertyName("ACTIVE")]
        public bool active { get; set; }

        [JsonPropertyName("CODE")]
        public string code { get; set; }

        [JsonPropertyName("COMPANY_NAME")]
        public string companyName { get; set; }

        [JsonPropertyName("ADDRESS")]
        public string? address { get; set; }

        [JsonPropertyName("ADDRESS2")]
        public string? address2 { get; set; }

        [JsonPropertyName("AREA1")]
        public string? area1 { get; set; }

        [JsonPropertyName("AREA2")]
        public string? area2 { get; set; }

        [JsonPropertyName("AREA3")]
        public string? area3 { get; set; }

        [JsonPropertyName("AREA4")]
        public string? area4 { get; set; }

        [JsonPropertyName("ZONE1")]
        public string? zone1 { get; set; }

        [JsonPropertyName("ZONE2")]
        public string? zone2 { get; set; }

        [JsonPropertyName("REGION1")]
        public string? region1 { get; set; }

        [JsonPropertyName("REGION2")]
        public string? region2 { get; set; }

        [JsonPropertyName("ZIP")]
        public string? zip { get; set; }

        [JsonPropertyName("COUNTRY")]
        public string? country { get; set; }

        [JsonPropertyName("CONTACT_NAME")]
        public string? contactName { get; set; }

        [JsonPropertyName("CONTACT_PHONE")]
        public string? contactPhone { get; set; }

        [JsonPropertyName("CONTACT_EMAIL")]
        public string? contactEmail { get; set; }

        [JsonPropertyName("NOTE")]
        public string? note { get; set; }

        [JsonPropertyName("REF_TYPE")]
        public CreateOrUpdatePartnerRqDataRefType[]? refType { get; set; }
    }
}
