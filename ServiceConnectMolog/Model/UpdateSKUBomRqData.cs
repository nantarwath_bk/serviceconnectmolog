﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class UpdateSKUBomRqData
    {

        [JsonPropertyName("SKU_CODE")]
        public string? skuCode { set; get; }

        [JsonPropertyName("CHILDREN")]
        public UpdateSKUBomRqDataChildren[]? children { set; get; }
    }
}
