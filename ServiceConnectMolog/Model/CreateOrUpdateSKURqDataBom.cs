﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdateSKURqDataBom
    {

        [JsonPropertyName("SKU_CHILD_CODE")]
        public bool skuChildCode { set; get; }

        [JsonPropertyName("QTY")]
        public bool qty { set; get; }
    }
}
