﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdatePartnerRqDataRefType
    {
        [JsonPropertyName("TYPE")]
        public string type { get; set; }

        [JsonPropertyName("INFORM_EMAIL")]
        public bool informEmail { get; set; }
    }
}
