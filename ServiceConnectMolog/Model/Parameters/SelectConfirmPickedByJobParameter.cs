﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography.Xml;
using System.Text.Json.Serialization;
using System.Xml.Linq;

namespace ServiceConnectMolog.Model.Parameters
{
    public class SelectConfirmPickedByJobParameter : BaseParameters
    {
        [JsonPropertyName("ACCESS_TOKEN")]
        [FromQuery(Name = "ACCESS_TOKEN")]
        public string accessToken { get; set; }

        [JsonPropertyName("<Release Reference Label-1>")]
        [FromQuery(Name = "<Release Reference Label-1>")]
        public string? releaseReferenceLabel1 { get; set; }

        [JsonPropertyName("<Release Reference Label-2>")]
        [FromQuery(Name = "<Release Reference Label-2>")]
        public string? releaseReferenceLabel2 { get; set; }

        [JsonPropertyName("<Release Reference Label-3>")]
        [FromQuery(Name = "<Release Reference Label-3>")]
        public string? releaseReferenceLabel3 { get; set; }

        [JsonPropertyName("<Release Reference Label-4>")]
        [FromQuery(Name = "<Release Reference Label-4>")]
        public string? releaseReferenceLabel4 { get; set; }

        [JsonPropertyName("INVOICE_NO")]
        [FromQuery(Name = "INVOICE_NO")]
        public string? invoiceNo { get; set; }
        
    }
}
