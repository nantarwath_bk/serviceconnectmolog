﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model.Parameters
{
    public class SelectinventorylistParameter : BaseParameters
    {
        [JsonPropertyName("ACCESS_TOKEN")]
        [FromQuery(Name = "ACCESS_TOKEN")]
        public string accessToken { get; set; }

        [JsonPropertyName("PAGE")]
        [FromQuery(Name = "PAGE")]
        public float page { get; set; }

        [JsonPropertyName("SIZE")]
        [FromQuery(Name = "SIZE")]
        public float size { get; set; }

        [JsonPropertyName("RECEIVED_DATE_FROM")]
        [FromQuery(Name = "RECEIVED_DATE_FROM")]
        public DateTime? receivedDateFrom { get; set; }

        [JsonPropertyName("RECEIVED_DATE_TO")]
        [FromQuery(Name = "RECEIVED_DATE_TO")]
        public DateTime? receivedDateTo { get; set; }

        [JsonPropertyName("SKU_CODE")]
        [FromQuery(Name = "SKU_CODE")]
        public string? skuCode { get; set; }

        [JsonPropertyName("ON_HAND_ONLY")]
        [FromQuery(Name = "ON_HAND_ONLY")]
        public bool? onHandOnly { get; set; }
    }
}
