﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model.Parameters
{
    public class BaseParameters
    {
        [JsonPropertyName("APP_KEY")]
        [FromQuery(Name = "APP_KEY")]
        public string appKey { get; set; }

        [JsonPropertyName("TIMESTAMP")]
        [FromQuery(Name = "TIMESTAMP")]
        public float timestamp { get; set; }

        [JsonPropertyName("SIGN")]
        [FromQuery(Name = "SIGN")]
        public string sign { get; set; }

        [JsonPropertyName("ACCESS_TOKEN")]
        [FromQuery(Name = "ACCESS_TOKEN")]
        public string accessToken { get; set; }
    }
}
