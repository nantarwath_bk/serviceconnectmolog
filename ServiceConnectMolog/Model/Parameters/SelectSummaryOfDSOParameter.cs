﻿using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model.Parameters
{
    public class SelectSummaryOfDSOParameter : BaseParameters
    {
        [JsonPropertyName("ACCESS_TOKEN")]
        [FromQuery(Name = "ACCESS_TOKEN")]
        public string accessToken { get; set; }

        [JsonPropertyName("CREATE_DATE_FROM")]
        [FromQuery(Name = "CREATE_DATE_FROM")]
        public DateTime createDateFrom { get; set; }

        [JsonPropertyName("CREATE_DATE_TO")]
        [FromQuery(Name = "CREATE_DATE_TO")]
        public DateTime createDateTo { get; set; }
    }
}
