﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateDSOJobRqDetail
    {
        [JsonPropertyName("SKU_CODE")]
        public string skuCode { get; set; }

        [JsonPropertyName("RELEASE_QTY")]
        public float releaseQty { get; set; }

        [JsonPropertyName("PRODUCT_TYPE")]
        public string? productType { get; set; }

        [JsonPropertyName("PACK_CODE")]
        public string? packCode { get; set; }

        [JsonPropertyName("PACK_QTY")]
        public float? packQty { get; set; }

        [JsonPropertyName("PALLET_ID")]
        public string? palletId { get; set; }

        [JsonPropertyName("CARTON_ID")]
        public string? cartonId { get; set; }

        [JsonPropertyName("EXPIRY_DATE")]
        public DateTime? expiryDate { get; set; }

        [JsonPropertyName("MANUF_DATE")]
        public DateTime? manufDate { get; set; }

        [JsonPropertyName("PICK_BY_CODE")]
        public string? pickByCode { get; set; }

        [JsonPropertyName("LOCATION_CODE")]
        public string? locationCode { get; set; }

        [JsonPropertyName("LOT_NO")]
        public string? lotNo { get; set; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { get; set; }

        [JsonPropertyName("DEBOM")]
        public bool? debom { get; set; }

        [JsonPropertyName("SYS_ID")]
        public string? sysId { get; set; }

        [JsonPropertyName("<Release User Defined-01>")]
        public string? releaseUserDefined01 { get; set; }

        [JsonPropertyName("<Release User Defined-02>")]
        public string? releaseUserDefined02 { get; set; }

        [JsonPropertyName("<Release User Defined-03>")]
        public string? releaseUserDefined03 { get; set; }

        [JsonPropertyName("<Release User Defined-04>")]
        public string? releaseUserDefined04 { get; set; }

        [JsonPropertyName("<Release User Defined-05>")]
        public string? releaseUserDefined05 { get; set; }

        [JsonPropertyName("<Release User Defined-06>")]
        public string? releaseUserDefined06 { get; set; }

        [JsonPropertyName("<Release User Defined-07>")]
        public string? releaseUserDefined07 { get; set; }

        [JsonPropertyName("<Release User Defined-08>")]
        public string? releaseUserDefined08 { get; set; }

        [JsonPropertyName("<Release User Defined-09>")]
        public string? releaseUserDefined09 { get; set; }

        [JsonPropertyName("<Release User Defined-10>")]
        public string? releaseUserDefined10 { get; set; }

        [JsonPropertyName("<Release User Defined-11>")]
        public string? releaseUserDefined11 { get; set; }

        [JsonPropertyName("<Release User Defined-12>")]
        public string? releaseUserDefined12 { get; set; }

        [JsonPropertyName("<Release User Defined-13>")]
        public string? releaseUserDefined13 { get; set; }

        [JsonPropertyName("<Release User Defined-14>")]
        public string? releaseUserDefined14 { get; set; }

        [JsonPropertyName("<Release User Defined-15>")]
        public string? releaseUserDefined15 { get; set; }

        [JsonPropertyName("<Release User Defined-16>")]
        public string? releaseUserDefined16 { get; set; }

        [JsonPropertyName("<Release User Defined-17>")]
        public string? releaseUserDefined17 { get; set; }

        [JsonPropertyName("<Release User Defined-18>")]
        public string? releaseUserDefined18 { get; set; }

        [JsonPropertyName("<Release User Defined-19>")]
        public string? releaseUserDefined19 { get; set; }

        [JsonPropertyName("<Release User Defined-20>")]
        public string? releaseUserDefined20 { get; set; }

        [JsonPropertyName("<Release User Defined-21>")]
        public string? releaseUserDefined21 { get; set; }

        [JsonPropertyName("<Release User Defined-22>")]
        public string? releaseUserDefined22 { get; set; }

        [JsonPropertyName("<Release User Defined-23>")]
        public string? releaseUserDefined23 { get; set; }

        [JsonPropertyName("<Release User Defined-24>")]
        public string? releaseUserDefined24 { get; set; }

        [JsonPropertyName("<Release User Defined-25>")]
        public string? releaseUserDefined25 { get; set; }

        [JsonPropertyName("<Release User Defined-26>")]
        public string? releaseUserDefined26 { get; set; }

        [JsonPropertyName("<Release User Defined-27>")]
        public string? releaseUserDefined27 { get; set; }

        [JsonPropertyName("<Release User Defined-28>")]
        public string? releaseUserDefined28 { get; set; }

        [JsonPropertyName("<Release User Defined-29>")]
        public string? releaseUserDefined29 { get; set; }

        [JsonPropertyName("<Release User Defined-30>")]
        public string? releaseUserDefined30 { get; set; }

    }
}
