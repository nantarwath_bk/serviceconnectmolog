﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateASNJobRqDetail
    {
        [JsonPropertyName("SKU_CODE")]
        public string skuCode { get; set; }

        [JsonPropertyName("RECEIVED_QTY")]
        public float receivedQty { get; set; }

        [JsonPropertyName("PRODUCT_TYPE")]
        public string? productType { get; set; }

        [JsonPropertyName("PALLET_ID")]
        public string? palletId { get; set; }

        [JsonPropertyName("CARTON_ID")]
        public string? cartonId { get; set; }

        [JsonPropertyName("MANUF_DATE")]
        public DateTime? manufDate { get; set; }

        [JsonPropertyName("EXPIRY_DATE")]
        public DateTime? expiryDate { get; set; }

        [JsonPropertyName("LENGTH")]
        public float? length { get; set; }

        [JsonPropertyName("WIDTH")]
        public float? width { get; set; }

        [JsonPropertyName("HEIGHT")]
        public float? height { get; set; }

        [JsonPropertyName("LENGTH_UOM")]
        public string? lengthUom { get; set; }

        [JsonPropertyName("VOLUME_UOM")]
        public string? volumeUom { get; set; }

        [JsonPropertyName("GROSS_WEIGHT")]
        public float? grossWeight { get; set; }

        [JsonPropertyName("NET_WEIGHT")]
        public float? netWeight { get; set; }

        [JsonPropertyName("WEIGHT_UOM")]
        public string? weightUom { get; set; }

        [JsonPropertyName("PACK_CODE")]
        public string? packCode { get; set; }

        [JsonPropertyName("PACK_QTY")]
        public float? packQty { get; set; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { get; set; }

        [JsonPropertyName("DEBOM")]
        public bool? debom { get; set; }

        [JsonPropertyName("LOT_NO")]
        public string? lotNo { get; set; }

        [JsonPropertyName("PRICE")]
        public float? price { get; set; }

        [JsonPropertyName("ERP_LINE_ITEM")]
        public string? erpLineItem { get; set; }

        [JsonPropertyName("<Receive User Defined-01>")]
        public string? receiveUserDefined01 { get; set; }
        [JsonPropertyName("<Receive User Defined-02>")]
        public string? receiveUserDefined02 { get; set; }
        [JsonPropertyName("<Receive User Defined-03>")]
        public string? receiveUserDefined03 { get; set; }
        [JsonPropertyName("<Receive User Defined-04>")]
        public string? receiveUserDefined04 { get; set; }
        [JsonPropertyName("<Receive User Defined-05>")]
        public string? receiveUserDefined05 { get; set; }
        [JsonPropertyName("<Receive User Defined-06>")]
        public string? receiveUserDefined06 { get; set; }
        [JsonPropertyName("<Receive User Defined-07>")]
        public string? receiveUserDefined07 { get; set; }
        [JsonPropertyName("<Receive User Defined-08>")]
        public string? receiveUserDefined08 { get; set; }
        [JsonPropertyName("<Receive User Defined-09>")]
        public string? receiveUserDefined09 { get; set; }
        [JsonPropertyName("<Receive User Defined-10>")]
        public string? receiveUserDefined10 { get; set; }
        [JsonPropertyName("<Receive User Defined-11>")]
        public string? receiveUserDefined11 { get; set; }
        [JsonPropertyName("<Receive User Defined-12>")]
        public string? receiveUserDefined12 { get; set; }
        [JsonPropertyName("<Receive User Defined-13>")]
        public string? receiveUserDefined13 { get; set; }
        [JsonPropertyName("<Receive User Defined-14>")]
        public string? receiveUserDefined14 { get; set; }
        [JsonPropertyName("<Receive User Defined-15>")]
        public string? receiveUserDefined15 { get; set; }
        [JsonPropertyName("<Receive User Defined-16>")]
        public string? receiveUserDefined16 { get; set; }
        [JsonPropertyName("<Receive User Defined-17>")]
        public string? receiveUserDefined17 { get; set; }
        [JsonPropertyName("<Receive User Defined-18>")]
        public string? receiveUserDefined18 { get; set; }
        [JsonPropertyName("<Receive User Defined-19>")]
        public string? receiveUserDefined19 { get; set; }
        [JsonPropertyName("<Receive User Defined-20>")]
        public string? receiveUserDefined20 { get; set; }
        [JsonPropertyName("<Receive User Defined-21>")]
        public string? receiveUserDefined21 { get; set; }
        [JsonPropertyName("<Receive User Defined-22>")]
        public string? receiveUserDefined22 { get; set; }
        [JsonPropertyName("<Receive User Defined-23>")]
        public string? receiveUserDefined23 { get; set; }
        [JsonPropertyName("<Receive User Defined-24>")]
        public string? receiveUserDefined24 { get; set; }
        [JsonPropertyName("<Receive User Defined-25>")]
        public string? receiveUserDefined25 { get; set; }
        [JsonPropertyName("<Receive User Defined-26>")]
        public string? receiveUserDefined26 { get; set; }
        [JsonPropertyName("<Receive User Defined-27>")]
        public string? receiveUserDefined27 { get; set; }
        [JsonPropertyName("<Receive User Defined-28>")]
        public string? receiveUserDefined28 { get; set; }
        [JsonPropertyName("<Receive User Defined-29>")]
        public string? receiveUserDefined29 { get; set; }
        [JsonPropertyName("<Receive User Defined-30>")]
        public string? receiveUserDefined30 { get; set; }
    }
}
