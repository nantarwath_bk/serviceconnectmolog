﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreateOrUpdateSKURqDataDimension
    {
        [JsonPropertyName("SKU_LEVEL")]
        public int skuLevel { set; get; }

        [JsonPropertyName("PACK_CODE")]
        public string packCode { set; get; }

        [JsonPropertyName("PACK_QTY")]
        public float packQty { set; get; }

        [JsonPropertyName("DEF_PACK")]
        public bool defPack { set; get; }

        [JsonPropertyName("LENGTH")]
        public float length { set; get; }

        [JsonPropertyName("WIDTH")]
        public float width { set; get; }

        [JsonPropertyName("HEIGHT")]
        public float height { set; get; }

        [JsonPropertyName("LENGTH_UOM")]
        public string lengthUom { set; get; }

        [JsonPropertyName("VOLUME")]
        public float volume { set; get; }

        [JsonPropertyName("VOLUME_UOM")]
        public string volumeUom { set; get; }

        [JsonPropertyName("NET_WEIGHT")]
        public float netWeight { set; get; }

        [JsonPropertyName("GROSS_WEIGHT")]
        public float grossWeight { set; get; }

        [JsonPropertyName("WEIGHT_UOM")]
        public string weightUom { set; get; }

        [JsonPropertyName("DEF_PACK_CAL")]
        public bool? defPackCal { set; get; }

        [JsonPropertyName("DEF_MKP")]
        public bool? defMkp { set; get; }

        [JsonPropertyName("PRICE")]
        public float? price { set; get; }

        [JsonPropertyName("DISCOUNT_BY")]
        public string? discountBy { set; get; }

        [JsonPropertyName("DISCOUNT_VALUE")]
        public float? discountValue { set; get; }

        [JsonPropertyName("BUBBLE_RATIO")]
        public float? bubleRatio { set; get; }

        [JsonPropertyName("SUG_PACKAGE_SIZE")]
        public int? sugPackageSize { set; get; }

        [JsonPropertyName("BARCODE")]
        public CreateOrUpdateSKURqDataDimensionBarcode[]? barcode { set; get; }

    }
}
