﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class CreatePSOJobRqDetail
    {
        [JsonPropertyName("SKU_CODE")]
        public string skuCode { set; get; }

        [JsonPropertyName("RELEASE_QTY")]
        public float releaseQty { set; get; }

        [JsonPropertyName("PRODUCT_TYPE")]
        public string? productType { set; get; }

        [JsonPropertyName("PACK_CODE")]
        public string? packCode { set; get; }

        [JsonPropertyName("PACK_QTY")]
        public float? packQty { set; get; }

        [JsonPropertyName("PALLET_ID")]
        public string? palletId { set; get; }

        [JsonPropertyName("CARTON_ID")]
        public string? cartonId { set; get; }

        [JsonPropertyName("EXPIRY_DATE")]
        public DateTime? expiryDate { set; get; }

        [JsonPropertyName("MANUF_DATE")]
        public DateTime? manufDate { set; get; }

        [JsonPropertyName("LOCATION_CODE")]
        public string? locationCode { set; get; }

        [JsonPropertyName("LOT_NO")]
        public string? lotNo { set; get; }

        [JsonPropertyName("REMARKS")]
        public string? remarks { set; get; }

        [JsonPropertyName("DEBOM")]
        public bool? debom { set; get; }

        [JsonPropertyName("SYS_ID")]
        public string? sysId { set; get; }


        [JsonPropertyName("<Release User Defined-01>")]
        public string? releaseUserDefined01 { set; get; }

        [JsonPropertyName("<Release User Defined-02>")]
        public string? releaseUserDefined02 { set; get; }

        [JsonPropertyName("<Release User Defined-03>")]
        public string? releaseUserDefined03 { set; get; }

        [JsonPropertyName("<Release User Defined-04>")]
        public string? releaseUserDefined04 { set; get; }

        [JsonPropertyName("<Release User Defined-05>")]
        public string? releaseUserDefined05 { set; get; }

        [JsonPropertyName("<Release User Defined-06>")]
        public string? releaseUserDefined06 { set; get; }

        [JsonPropertyName("<Release User Defined-07>")]
        public string? releaseUserDefined07 { set; get; }

        [JsonPropertyName("<Release User Defined-08>")]
        public string? releaseUserDefined08 { set; get; }

        [JsonPropertyName("<Release User Defined-09>")]
        public string? releaseUserDefined09 { set; get; }

        [JsonPropertyName("<Release User Defined-10>")]
        public string? releaseUserDefined10 { set; get; }

        [JsonPropertyName("<Release User Defined-11>")]
        public string? releaseUserDefined11 { set; get; }

        [JsonPropertyName("<Release User Defined-12>")]
        public string? releaseUserDefined12 { set; get; }

        [JsonPropertyName("<Release User Defined-13>")]
        public string? releaseUserDefined13 { set; get; }

        [JsonPropertyName("<Release User Defined-14>")]
        public string? releaseUserDefined14 { set; get; }

        [JsonPropertyName("<Release User Defined-15>")]
        public string? releaseUserDefined15 { set; get; }

        [JsonPropertyName("<Release User Defined-16>")]
        public string? releaseUserDefined16 { set; get; }

        [JsonPropertyName("<Release User Defined-17>")]
        public string? releaseUserDefined17 { set; get; }

        [JsonPropertyName("<Release User Defined-18>")]
        public string? releaseUserDefined18 { set; get; }

        [JsonPropertyName("<Release User Defined-19>")]
        public string? releaseUserDefined19 { set; get; }

        [JsonPropertyName("<Release User Defined-20>")]
        public string? releaseUserDefined20 { set; get; }

        [JsonPropertyName("<Release User Defined-21>")]
        public string? releaseUserDefined21 { set; get; }

        [JsonPropertyName("<Release User Defined-22>")]
        public string? releaseUserDefined22 { set; get; }

        [JsonPropertyName("<Release User Defined-23>")]
        public string? releaseUserDefined23 { set; get; }

        [JsonPropertyName("<Release User Defined-24>")]
        public string? releaseUserDefined24 { set; get; }

        [JsonPropertyName("<Release User Defined-25>")]
        public string? releaseUserDefined25 { set; get; }

        [JsonPropertyName("<Release User Defined-26>")]
        public string? releaseUserDefined26 { set; get; }

        [JsonPropertyName("<Release User Defined-27>")]
        public string? releaseUserDefined27 { set; get; }

        [JsonPropertyName("<Release User Defined-28>")]
        public string? releaseUserDefined28 { set; get; }

        [JsonPropertyName("<Release User Defined-29>")]
        public string? releaseUserDefined29 { set; get; }

        [JsonPropertyName("<Release User Defined-30>")]
        public string? releaseUserDefined30 { set; get; }

        [JsonPropertyName("<Receive User Defined-01>")]
        public string? receiveUserDefined01 { set; get; }

        [JsonPropertyName("<Receive User Defined-02>")]
        public string? receiveUserDefined02 { set; get; }

        [JsonPropertyName("<Receive User Defined-03>")]
        public string? receiveUserDefined03 { set; get; }

        [JsonPropertyName("<Receive User Defined-04>")]
        public string? receiveUserDefined04 { set; get; }

        [JsonPropertyName("<Receive User Defined-05>")]
        public string? receiveUserDefined05 { set; get; }

        [JsonPropertyName("<Receive User Defined-06>")]
        public string? receiveUserDefined06 { set; get; }

        [JsonPropertyName("<Receive User Defined-07>")]
        public string? receiveUserDefined07 { set; get; }

        [JsonPropertyName("<Receive User Defined-08>")]
        public string? receiveUserDefined08 { set; get; }

        [JsonPropertyName("<Receive User Defined-09>")]
        public string? receiveUserDefined09 { set; get; }

        [JsonPropertyName("<Receive User Defined-10>")]
        public string? receiveUserDefined10 { set; get; }

        [JsonPropertyName("<Receive User Defined-11>")]
        public string? receiveUserDefined11 { set; get; }

        [JsonPropertyName("<Receive User Defined-12>")]
        public string? receiveUserDefined12 { set; get; }

        [JsonPropertyName("<Receive User Defined-13>")]
        public string? receiveUserDefined13 { set; get; }

        [JsonPropertyName("<Receive User Defined-14>")]
        public string? receiveUserDefined14 { set; get; }

        [JsonPropertyName("<Receive User Defined-15>")]
        public string? receiveUserDefined15 { set; get; }

        [JsonPropertyName("<Receive User Defined-16>")]
        public string? receiveUserDefined16 { set; get; }

        [JsonPropertyName("<Receive User Defined-17>")]
        public string? receiveUserDefined17 { set; get; }

        [JsonPropertyName("<Receive User Defined-18>")]
        public string? receiveUserDefined18 { set; get; }

        [JsonPropertyName("<Receive User Defined-19>")]
        public string? receiveUserDefined19 { set; get; }

        [JsonPropertyName("<Receive User Defined-20>")]
        public string? receiveUserDefined20 { set; get; }

        [JsonPropertyName("<Receive User Defined-21>")]
        public string? receiveUserDefined21 { set; get; }

        [JsonPropertyName("<Receive User Defined-22>")]
        public string? receiveUserDefined22 { set; get; }

        [JsonPropertyName("<Receive User Defined-23>")]
        public string? receiveUserDefined23 { set; get; }

        [JsonPropertyName("<Receive User Defined-24>")]
        public string? receiveUserDefined24 { set; get; }

        [JsonPropertyName("<Receive User Defined-25>")]
        public string? receiveUserDefined25 { set; get; }

        [JsonPropertyName("<Receive User Defined-26>")]
        public string? receiveUserDefined26 { set; get; }

        [JsonPropertyName("<Receive User Defined-27>")]
        public string? receiveUserDefined27 { set; get; }

        [JsonPropertyName("<Receive User Defined-28>")]
        public string? receiveUserDefined28 { set; get; }

        [JsonPropertyName("<Receive User Defined-29>")]
        public string? receiveUserDefined29 { set; get; }

        [JsonPropertyName("<Receive User Defined-30>")]
        public string? receiveUserDefined30 { set; get; }

    }
}
