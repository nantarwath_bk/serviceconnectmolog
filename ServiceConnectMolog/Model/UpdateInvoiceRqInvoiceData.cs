﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class UpdateInvoiceRqInvoiceData
    {
        [JsonPropertyName("PAYMENT_TYPE")]
        public string? paymentType { get; set; }

        [JsonPropertyName("INV_FORMAT_FILE_DESTINATION")]
        public string? invFormatFileDestination { get; set; }

        [JsonPropertyName("INVOICE_DATE")]
        public DateTime? invoiceDate { get; set; }

        [JsonPropertyName("SALE_ORDER_NO")]
        public string? saleOrderNo { get; set; }

        [JsonPropertyName("CUSTOMER_PHONE_float")]
        public string? customerPhonefloat { get; set; }

        [JsonPropertyName("POSTING_DATE")]
        public DateTime? postingDate { get; set; }

        [JsonPropertyName("DUE_DATE")]
        public DateTime? dueDate { get; set; }

        [JsonPropertyName("CUSTOMER_PURCHASE_ORDER_REFERENCE")]
        public string? customerPurchaseOrderReference { get; set; }

        [JsonPropertyName("CUSTOMER_ID")]
        public string? customerId { get; set; }

        [JsonPropertyName("CUSTOMER_CODE")]
        public string? customerCode { get; set; }

        [JsonPropertyName("CUSTOMER_TAX_ID")]
        public string? customerTaxId { get; set; }

        [JsonPropertyName("CUSTOMER_BRANCH")]
        public string? customerBranch { get; set; }

        [JsonPropertyName("CUSTOMER_NAME")]
        public string? customerName { get; set; }

        [JsonPropertyName("CUSTOMER_EMAIL")]
        public string? customerEmail { get; set; }

        [JsonPropertyName("BILL_TO_NAME")]
        public string? billToName { get; set; }

        [JsonPropertyName("BILL_TO_CUSTOMER_ID")]
        public string? billToCustomerId { get; set; }

        [JsonPropertyName("BILL_TO_CUSTOMER_float")]
        public string? billToCustomerfloat { get; set; }

        [JsonPropertyName("BILL_TO_ADDRESS_1")]
        public string? billToAddress1 { get; set; }

        [JsonPropertyName("BILL_TO_ADDRESS_2")]
        public string? billToAddress2 { get; set; }

        [JsonPropertyName("BILL_TO_CITY")]
        public string? billToCity { get; set; }

        [JsonPropertyName("BILL_TO_COUNTRY")]
        public string? billToCountry { get; set; }

        [JsonPropertyName("BILL_TO_STATE")]
        public string? billToState { get; set; }

        [JsonPropertyName("BILL_TO_POST_CODE")]
        public string? billToPostCode { get; set; }

        [JsonPropertyName("SHIP_TO_CODE")]
        public string? shipToCode { get; set; }

        [JsonPropertyName("SHIP_TO_NAME")]
        public string? shipToName { get; set; }

        [JsonPropertyName("SHIP_TO_CONTACT")]
        public string? shipToContact { get; set; }

        [JsonPropertyName("SHIP_TO_ADDRESS_1")]
        public string? shipToAddress1 { get; set; }

        [JsonPropertyName("SHIP_TO_ADDRESS_2")]
        public string? shipToAddress2 { get; set; }

        [JsonPropertyName("SHIP_TO_CITY")]
        public string? shipToCity { get; set; }

        [JsonPropertyName("SHIP_TO_COUNTRY")]
        public string? shipToCountry { get; set; }

        [JsonPropertyName("SHIP_TO_STATE")]
        public string? shipToState { get; set; }

        [JsonPropertyName("SHIP_TO_POST_CODE")]
        public string? shipToPostCode { get; set; }

        [JsonPropertyName("CURRENCY_CODE")]
        public string? currencyCode { get; set; }

        [JsonPropertyName("PAYMENT_TERM")]
        public string? paymentTerm { get; set; }

        [JsonPropertyName("SALES_PERSON")]
        public string? salesPerson { get; set; }

        [JsonPropertyName("TAX_CODE")]
        public string? taxCode { get; set; }

        [JsonPropertyName("TAX_PERCENT")]
        public float? taxPercent { get; set; }

        [JsonPropertyName("PRICES_INCLUDE_TAX")]
        public bool? pricesIncludeTax { get; set; }

        [JsonPropertyName("REMAINING_AMOUNT")]
        public float? remainingAmount { get; set; }

        [JsonPropertyName("TOTAL_AMOUNT_BEFORE_DISCOUNT")]
        public float? totalAmountBeforeDiscount { get; set; }

        [JsonPropertyName("DISCOUNT_AMOUNT")]
        public float? discountAmount { get; set; }

        [JsonPropertyName("DISCOUNT_APPLIED_BEFORE_TAX")]
        public bool? discountAppliedBeforeTax { get; set; }

        [JsonPropertyName("TOTAL_AMOUNT_BEFORE_TAX")]
        public float? totalAmountBeforeTax { get; set; }

        [JsonPropertyName("TOTAL_TAX_AMOUNT")]
        public float? totalTaxAmount { get; set; }

        [JsonPropertyName("TOTAL_AMOUNT_INCLUDING_TAX")]
        public float? totalAmountIncludingTax { get; set; }

        [JsonPropertyName("DEPOSIT_AMOUNT")]
        public float? depositAmount { get; set; }

        [JsonPropertyName("STATUS")]
        public string? status { get; set; }

        [JsonPropertyName("<Release Header User Defined-01>")]
        public string? releaseHeaderUserDefined01 { get; set; }

        [JsonPropertyName("<Release Header User Defined-02>")]
        public string? releaseHeaderUserDefined02 { get; set; }

        [JsonPropertyName("<Release Header User Defined-03>")]
        public string? releaseHeaderUserDefined03 { get; set; }

        [JsonPropertyName("<Release Header User Defined-04>")]
        public string? releaseHeaderUserDefined04 { get; set; }

        [JsonPropertyName("<Release Header User Defined-05>")]
        public string? releaseHeaderUserDefined05 { get; set; }

        [JsonPropertyName("<Release Header User Defined-06>")]
        public string? releaseHeaderUserDefined06 { get; set; }

        [JsonPropertyName("<Release Header User Defined-07>")]
        public string? releaseHeaderUserDefined07 { get; set; }

        [JsonPropertyName("<Release Header User Defined-08>")]
        public string? releaseHeaderUserDefined08 { get; set; }

        [JsonPropertyName("<Release Header User Defined-09>")]
        public string? releaseHeaderUserDefined09 { get; set; }

        [JsonPropertyName("<Release Header User Defined-10>")]
        public string? releaseHeaderUserDefined10 { get; set; }

        [JsonPropertyName("<Release Header User Defined-11>")]
        public string? releaseHeaderUserDefined11 { get; set; }

        [JsonPropertyName("<Release Header User Defined-12>")]
        public string? releaseHeaderUserDefined12 { get; set; }

        [JsonPropertyName("<Release Header User Defined-13>")]
        public string? releaseHeaderUserDefined13 { get; set; }

        [JsonPropertyName("<Release Header User Defined-14>")]
        public string? releaseHeaderUserDefined14 { get; set; }

        [JsonPropertyName("<Release Header User Defined-15>")]
        public string? releaseHeaderUserDefined15 { get; set; }

        [JsonPropertyName("DETAIL")]
        public UpdateInvoiceRqInvoiceDataDetail? detail { get; set; }
    }
}
