﻿using System.Text.Json.Serialization;

namespace ServiceConnectMolog.Model
{
    public class UpdateSKUBomRqDataChildren
    {

        [JsonPropertyName("CHILD_SKU_CODE")]
        public string? childSkuCode { set; get; }

        [JsonPropertyName("QTY")]
        public float? qty { set; get; }
    }
}
